########################################################################
#
# Instructions to run locally:
#
# Run the following command in the directory
# of this file to build a docker image.
#
# docker build -t seaofnotes:latest .
#
# Shoot up the image with the following command,
# to run the tomcat server on the indicated ports!
#
# docker run -itd -p 8080:8080 --name seaofnotes seaofnotes:latest
#
########################################################################

#Actual Dockerfile
#Specify base-image
FROM tomcat:8.5

#add the author
MAINTAINER the barracudas

# Do what needs to be done
# RUN Commands within Docker
RUN ["sh", "-c", "rm -fr /usr/local/tomcat/webapps/ROOT"]
RUN ["sh", "-c", "rm -R -- /usr/local/tomcat/webapps/*"]

# ADD files
ADD target/seaofnotes.war /usr/local/tomcat/webapps/ROOT.war

EXPOSE 8080

# Define the default command to run once the image gets started
CMD ["catalina.sh", "run"]
