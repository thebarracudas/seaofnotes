# README #

#### Continuous Integration Status ####
[![CircleCI](https://circleci.com/bb/thebarracudas/seaofnotes.svg?style=svg)](https://circleci.com/bb/thebarracudas/seaofnotes)

#### Continuous Quality Analysis Status ####
http://ec2-54-149-147-189.us-west-2.compute.amazonaws.com:9000/dashboard/index/2

### What is this repository for? ###

* This repository includes a little Project for the master class Software Process Management from the Free University of Bolzano. The project is a team effort by Christine Lunger, Danilo Fink, Marco Pomalo and Marius Meisterernst.
* The goal of the project is to create a Continuous Integration Pipeline.
* Further information will follow.
* 0.0.@
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* to be defined later

### Contribution guidelines ###

* tbd
* Examples
    * Writing tests
    * Code review
    * Other guidelines

### Who do I talk to? ###

* any of the team members!

    Teammember          | E-Mail
    --------------------| --------------------------------
    Christine Lunger    | christine.lunger@stud-inf.unibz.it
    Danilo Fink         | danilo.fink@stud-inf.unibz.it
    Marius Meisterernst | marius.meisterernst@stud-inf.unibz.it
    Marco Pomalo        | marco.pomalo@stud-inf.unibz.it

### HISTORIC DAYS ###

* 2016.10.20 - The Barracudas start the project
* 2016.10.27 - The project runs on everybody's machine
* 2016.11.03 - First Unit Test has been designed, implemented and failed
* 2016.11.07 - First online build on drone.io
* 2016.11.21 - First cycle of our continuous analysis with SonarQube.
* 2017.01.04 - First time Sea of Notes works from a Dockerfile
* 2017.01.13 - Friday, build no. 100, Continuous deployment is finally working.
* WHAT is the next achievement?
