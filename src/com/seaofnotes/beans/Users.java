package com.seaofnotes.beans;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table (name = "USERS")
public class Users {
	
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	@Column (name = "ID", nullable = false, unique = true)
	private long id;
	
	@Column (name = "NAME", nullable = false)
	private String name;
	
	@Column (name = "SURNAME", nullable = false)
	private String surname;
	
	@Column (name = "EMAIL", nullable = false, unique = true)
	private String email;
	
	@Column (name = "PASSWORD", nullable = false)
	private String password;
	
	@Temporal (TemporalType.TIMESTAMP)
	@Column (name = "CREATED_AT", nullable = false)
	private Date createdAt;
	
	@Temporal (TemporalType.TIMESTAMP)
	@Column (name = "UPDATED_AT")
	private Date updatedAt;
	
	@Column (name = "ACTIVATED", nullable = false)
	private boolean activated;
	
	@Temporal (TemporalType.TIMESTAMP)
	@Column (name = "ACTIVATED_AT")
	private Date activatedAt;
	
	
	/*
	 * Getters and Setters
	 */

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public boolean isActivated() {
		return activated;
	}

	public void setActivated(boolean activated) {
		this.activated = activated;
	}

	public Date getActivatedAt() {
		return activatedAt;
	}

	public void setActivatedAt(Date activatedAt) {
		this.activatedAt = activatedAt;
	}	

}
