package com.seaofnotes.beans;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table (name = "POSTS")
public class Posts {
	
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	@Column (name = "ID", nullable = false, unique = true)
	private long id;
	
	@Temporal (TemporalType.TIMESTAMP)
	@Column (name = "POSTED_AT")
	private Date postedAt;
	
	@Column (name = "TEXT")
	private String text;

	@ManyToOne (cascade = CascadeType.MERGE)
	@JoinColumn (name = "POSTER_ID")
	private Users poster;
	
	/*
	 * Getters and Setters
	 */

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getPostedAt() {
		return postedAt;
	}

	public void setPostedAt(Date postedAt) {
		this.postedAt = postedAt;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Users getPoster() {
		return poster;
	}

	public void setPoster(Users poster) {
		this.poster = poster;
	}
	
	
	
}

