package com.seaofnotes.controllers;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.seaofnotes.beans.Users;
import com.seaofnotes.managers.UserManager;
import com.seaofnotes.util.LoginUtil;
import com.seaofnotes.util.SessionUtil;

/**
 * Servlet implementation class Settings
 */
@WebServlet("/Settings")
public class Settings extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Settings() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("title", "Settings");
		request.setAttribute("style", "settings");
		request.setAttribute("script", "settings");
		request.setAttribute("navs", true);
		request.setAttribute("isLI", LoginUtil.isLoggedIn(request));
		
		
		
		request.setAttribute("errorProfile", request.getSession().getAttribute("errorProfile"));
		request.setAttribute("successProfile", request.getSession().getAttribute("successProfile"));
		request.setAttribute("errorPassword", request.getSession().getAttribute("errorPassword"));
		request.setAttribute("successPassword", request.getSession().getAttribute("successPassword"));
		request.getSession().removeAttribute("errorProfile");
		request.getSession().removeAttribute("successProfile");
		request.getSession().removeAttribute("errorPassword");
		request.getSession().removeAttribute("successPassword");
		
		Users cUser = SessionUtil.getCurrentUserFromDB(request);
		request.setAttribute("cUser", cUser);
		
		RequestDispatcher rd = request.getRequestDispatcher("/views/settings.jsp");
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		if (request.getParameter("password-update") != null && request.getParameter("password-update").equals("true")) {
			UserManager um = new UserManager();
			um.updatePassword(request);
			response.sendRedirect(request.getContextPath() + "/Settings");
		}
		
		if (request.getParameter("profile-update") != null && request.getParameter("profile-update").equals("true")) {
			UserManager um = new UserManager();
			um.updateProfile(request);
			response.sendRedirect(request.getContextPath() + "/Settings");
		}
	}

}
