package com.seaofnotes.controllers;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.seaofnotes.managers.UserManager;
import com.seaofnotes.util.LoginUtil;
import com.seaofnotes.util.ValidationUserUtil;

/**
 * Servlet implementation class SignUp
 */
@WebServlet("/SignUp")
public class SignUp extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SignUp() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if (LoginUtil.isLoggedIn(request)) {
	    	response.sendRedirect(request.getContextPath() + "/Home");
		} else {
			request.setAttribute("title", "Sign Up");
			request.setAttribute("style", "signup");
			request.setAttribute("script", "signup");
			request.setAttribute("navs", true);
			RequestDispatcher rd = request.getRequestDispatcher("/views/signup.jsp");
			rd.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if (LoginUtil.isLoggedIn(request)) {
	    	response.sendRedirect(request.getContextPath() + "/Home");
	    	return;
		}
		
if (ValidationUserUtil.isSignUpValid(request)) {			
			
			UserManager um = new UserManager();
			um.saveUser(request);
	    	response.sendRedirect(request.getContextPath() + "/Home");
		} 
		else {
			request.setAttribute("title", "Sign Up");
			request.setAttribute("style", "signup");
			request.setAttribute("script", "signup");
			request.setAttribute("navs", true);
			request.setAttribute("errorSignup", "<span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span> Check your data and try again! Some fields are empty or have not been accepted.");
			RequestDispatcher rd = request.getRequestDispatcher("/views/signup.jsp");
			rd.forward(request, response);
		}
	}

}
