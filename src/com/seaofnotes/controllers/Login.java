package com.seaofnotes.controllers;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.seaofnotes.beans.Users;
import com.seaofnotes.util.LoginUtil;
import com.seaofnotes.util.SessionUtil;
import com.seaofnotes.util.ValidationUserUtil;

/**
 * Servlet implementation class Login
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if (LoginUtil.isLoggedIn(request)) {
			
	    	response.sendRedirect(request.getContextPath() + "/Home");
	    	return;
		} else {
			request.setAttribute("title", "Log In");
			request.setAttribute("style", "login");
			request.setAttribute("script", "login");
			request.setAttribute("navs", true);
			RequestDispatcher rd = request.getRequestDispatcher("/views/login.jsp");
			rd.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if (LoginUtil.isLoggedIn(request)) {
			
	    	response.sendRedirect(request.getContextPath() + "/Home");
	    	return;
		}
	    
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		
		if (ValidationUserUtil.isLoginValid(request)) {
			Users current = LoginUtil.authorize(email, password);
			if (current != null) {
				if (!current.isActivated()) {
			    	request.getSession().setAttribute("successHome", "<span class=\"glyphicon glyphicon-ok-sign\" aria-hidden=\"true\"></span> User active again.");
				}
			    	
				SessionUtil.setCurrentUser(current, request);
				
				response.sendRedirect(request.getContextPath() + "/Home");
				return;	
			}
			else
				request.setAttribute("errorLoginForm", "<span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span> Wrong Password or User does not exist.");
		}
		else
			request.setAttribute("errorLoginForm", "<span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span> Check your data and try again! Some fields are empty or have not been accepted.");
		
		request.setAttribute("title", "Log In");
		request.setAttribute("style", "login");
		request.setAttribute("script", "login");
		request.setAttribute("navs", true);
		RequestDispatcher rd = request.getRequestDispatcher("/views/login.jsp");
		rd.forward(request, response);
	}

}
