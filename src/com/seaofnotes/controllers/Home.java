package com.seaofnotes.controllers;

import java.io.IOException;
import java.rmi.UnknownHostException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.seaofnotes.beans.Posts;
import com.seaofnotes.managers.PostManager;
import com.seaofnotes.util.GeneralUtil;
import com.seaofnotes.util.LoginUtil;
import com.seaofnotes.util.ValidationPostUtil;

/**
 * Servlet implementation class Home
 */
@WebServlet("/Home")
public class Home extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Home() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setAttribute("successHome", request.getSession().getAttribute("successHome"));
		request.getSession().removeAttribute("successHome");
	

		request.setAttribute("starting", 0);
		PostManager pm = new PostManager();
		int psize = pm.getAllPosts().size();
		request.setAttribute("psize", psize);

		try{

			List<Posts> plist = PostManager.getPosts(0);

			request.setAttribute("plist", plist);
			request.setAttribute("isLI", LoginUtil.isLoggedIn(request));
			request.setAttribute("title", "Welcome");
			request.setAttribute("style", "home");
			request.setAttribute("script", "home");
			request.setAttribute("navs", true);
			RequestDispatcher rd = request.getRequestDispatcher("/views/home.jsp");
			rd.forward(request, response);			
		}
		catch(UnknownHostException uhex){
			System.out.println("Getting Posts or getting RequestDispatcher failed." + uhex);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setAttribute("title", "Welcome");
		request.setAttribute("style", "home");
		request.setAttribute("script", "home");
		request.setAttribute("navs", true);
		request.setAttribute("isLI", LoginUtil.isLoggedIn(request));
		PostManager pm = new PostManager();
		int psize = 0;
		
		if (LoginUtil.isLoggedIn(request)) {

			if ((request.getParameter("next") != null && request.getParameter("next").equals("true")) || (request.getParameter("prev") != null && request.getParameter("prev").equals("true"))) {

				int starting = GeneralUtil.getInteger(request.getParameter("starting")) != null ? GeneralUtil.getInteger(request.getParameter("starting")) : 0;
				
				List<Posts> plist = PostManager.getPosts(starting);
				psize = pm.getAllPosts().size();
				request.setAttribute("psize", psize);
				request.setAttribute("starting", starting);
				request.setAttribute("starting", starting);
				request.setAttribute("plist", plist);
				
				RequestDispatcher rd = request.getRequestDispatcher("/views/home.jsp");
				rd.forward(request, response);
			}

			else if (ValidationPostUtil.isPostValid(request)) {
				
				pm.savePost(request);
				
				List<Posts> plist = PostManager.getPosts(0);
				request.setAttribute("plist", plist);
				psize = pm.getAllPosts().size();
				request.setAttribute("psize", psize);
				request.setAttribute("starting", 0);
				RequestDispatcher rd = request.getRequestDispatcher("/views/home.jsp");
				rd.forward(request, response);
			

				
			} else {
				List<Posts> plist = PostManager.getPosts(0);
				request.setAttribute("plist", plist);
				request.setAttribute("psize", psize);
				request.setAttribute("starting", 0);
				request.setAttribute("errorHome", "<span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span> Post not valid.");
				
				RequestDispatcher rd = request.getRequestDispatcher("/views/home.jsp");
				rd.forward(request, response);
			}

		}

	}

}
