package com.seaofnotes.util;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.seaofnotes.beans.Users;

public class LoginUtil {
	
	public static Users authorize(String email, String password) {
		
		Session session = HibernateUtil.getSessionFactory().openSession();
    	session.beginTransaction();
    	
    	@SuppressWarnings("deprecation")
		Users current  = (Users) session.createCriteria(Users.class)
							.add(Restrictions.eq("email", email))
							.uniqueResult();
    	
    	session.getTransaction().commit();
    	
    	if (current != null && current.getPassword().equals(GeneralUtil.generateHash(password)))
	    	return current;
    	
    	return null;
	}
	
	public static boolean isLoggedIn(HttpServletRequest request) {
		return SessionUtil.isCurrentUser(request);	
	}
	

}
