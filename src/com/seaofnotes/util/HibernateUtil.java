package com.seaofnotes.util;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {

	private static final SessionFactory sessionFactory = buildSessionFactory();

	private static SessionFactory buildSessionFactory() {
        try {
            // Create the SessionFactory from hibernate.cfg.xml
        	String env = System.getenv("run_mode");
    		
    		
    		String dbServer = "jdbc:postgresql://db:5432/";
    		String dbValue = "seaofnotes";
    		
    		Configuration hConf = new Configuration().configure();
    		
    		
    		 if ((env == "") || (env == null)){
    			dbServer = "jdbc:postgresql://db:5432/";
    			dbValue = "seaofnotes";
    			System.out.println("DB = "+dbServer+dbValue);
    		}
    		
    		
    		if ("dev".equals(env)){
    			dbServer = "jdbc:postgresql://54.201.85.122:5432/";
    			dbValue = "seaofnotes_dev";
    			System.out.println("DB = "+dbServer+dbValue);
    		}
    		if ("prod".equals(env)){
    			dbServer = "jdbc:postgresql://54.201.85.122:5432/";
    			dbValue = "seaofnotes_prod";
    			System.out.println("DB = "+dbServer+dbValue);
    		}
    		
    		hConf.setProperty("hibernate.connection.url", dbServer+dbValue);
            return hConf.buildSessionFactory();

        }
     catch (RuntimeException ex) { //Throwable ausgetauscht, da man die nicht catched.
            // Make sure you log the exception, as it might be swallowed
            System.err.println("Initial SessionFactory creation failed. :-( " + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

	public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public static void shutdown() {
    	// Close caches and connection pools
    	getSessionFactory().close();
    }


}
