package com.seaofnotes.util;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.apache.commons.validator.routines.EmailValidator;

import com.seaofnotes.beans.Users;

public class ValidationUserUtil {
	
	
	public static boolean isSignUpValid(HttpServletRequest request) {
		return (isNameValid(request.getParameter("name")) &&
				isNameValid(request.getParameter("surname")) &&
				isEmailValid(request.getParameter("email")) &&
				isEmailFree(request.getParameter("email")) &&
				isNewPasswordValid(request.getParameter("password"), request.getParameter("password_retype")));
	}


	public static boolean isEditUserValid(HttpServletRequest request) {
		return (isNameValid(request.getParameter("name")) &&
				isNameValid(request.getParameter("surname")) &&
				isEmailValid(request.getParameter("email")));
	}


	public static boolean isLoginValid(HttpServletRequest request) {
		return (isEmailValid(request.getParameter("email")) &&
				isPasswordValid(request.getParameter("password")));
	}


	public static boolean isProfileValid(HttpServletRequest request) {
		return (isNameValid(request.getParameter("name")) &&
				isNameValid(request.getParameter("surname")) &&
				isEmailValid(request.getParameter("email")) &&
				isOldPasswordValid(request.getParameter("password"), SessionUtil.getCurrentUser(request)));
	}


	/*
	 * Validators
	 */
	public static boolean isEmailValid(String email) {
		return (email != null && email.length() != 0 && EmailValidator.getInstance().isValid(email));
	}

	public static boolean isEmailFree(String email) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();

		@SuppressWarnings("deprecation")
		Users user  = (Users) session.createCriteria(Users.class)
				.add(Restrictions.eq("email", email))
				.uniqueResult();

		session.getTransaction().commit();

		return (user != null) ? false : true;
	}

	public static boolean isNameValid(String name) {
		boolean valid;

		if (name != null && name.length() != 0)
			valid = name.matches("[a-zA-z]+([ '-][a-zA-Z]+)*");

		else
			valid = false;

		return valid;
	}

	public static boolean isDateValid(String date) {
		if (date != null && date.length() != 0 && DateTimeUtil.getDateFromString(date) != null)
			return true;
		else
			return false;
	}

	public static boolean isPasswordValid(String password) {		
		boolean valid = false;

		if (password != null && password.length() != 0) {
			valid = password.matches("((?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{8,20})");
		} else{
			valid = false;
		}
		return valid;
	}

	public static boolean isNewPasswordValid(String firstPassword, String secondPassword) {
		return (isPasswordValid(firstPassword) && isPasswordValid(secondPassword) && firstPassword.equals(secondPassword));
	}

	public static boolean isOldPasswordValid(String oldPassword, Users current) {
		return (isPasswordValid(oldPassword) && current.getPassword().equals(GeneralUtil.generateHash(oldPassword)));
	}

}
