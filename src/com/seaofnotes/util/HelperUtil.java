package com.seaofnotes.util;

public class HelperUtil {
	
	/**
	 * Get valid String or null from String
	 */
	public static String getString(String s) {
		return (s != null && !s.isEmpty()) ? s : null;
	}

}
