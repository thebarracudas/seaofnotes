package com.seaofnotes.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

public class GeneralUtil {
	
	/**
	 * Get Long (or null) from String
	 */
	public static Long getLong(String s) {
		return isLong(s) ? Long.parseLong(s) : null;
	}
	
	/**
	 * Get Integer (or null) from String
	 */
	public static Integer getInteger(String s) {
		return isInteger(s) ? Integer.parseInt(s) : null;
	}
	
	/**
	 * Get Double (or null) from String
	 */
	public static Double getDouble(String s) {
		return isDouble(s) ? Double.parseDouble(s.replace(",", ".")) : null;
	}
	
	/**
	 * Get String (or null) from String
	 */
	public static String getString(String s) {
		return (s != null && !s.isEmpty()) ? s : null;
	}
	
	/**
	 * Get Boolean from String
	 */
	public static boolean getBool(String s){
		return (s != null) ? true : false;
	}
	
	
	public static boolean isInteger(String s) {
	    try { 
	        Integer.parseInt(s); 
	    } catch(NumberFormatException e) { 
	        return false; 
	    } catch(NullPointerException e) {
	        return false;
	    }
	    // only got here if we didn't return false
	    return true;
	}
	
	public static boolean isDouble(String s) {
	    try { 
	        Double.parseDouble(s); 
	    } catch(NumberFormatException e) { 
	        return false; 
	    } catch(NullPointerException e) {
	        return false;
	    }
	    // only got here if we didn't return false
	    return true;
	}
	
	public static boolean isLong(String s) {
	    try { 
	        Long.parseLong(s); 
	    } catch(NumberFormatException e) { 
	        return false; 
	    } catch(NullPointerException e) {
	        return false;
	    }
	    // only got here if we didn't return false
	    return true;
	}
	
	public static String removeComma(String s) {
		return s.replace(",", "");
	}
	
	public static String generateUUID () {
		return UUID.randomUUID().toString();
	}
	
	public static String generateHash(String password) {
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("SHA-256");
			md.update(password.getBytes());
			byte[] digest = md.digest();
			return String.format("%064x", new java.math.BigInteger(1, digest));
		} catch (NoSuchAlgorithmException e) {
			return null;
		}
	}

}
