package com.seaofnotes.util;

import javax.servlet.http.HttpServletRequest;

public class ValidationPostUtil {
	
	public static boolean isPostValid(HttpServletRequest request) {
		return (isTextLengthValid(request.getParameter("message")) &&
				LoginUtil.isLoggedIn(request));
	}
	
	
	/*
	 * Validators
	 */
	public static boolean isTextValid(String text) {
		boolean valid;
		
		if (text != null && text.length() != 0)
			valid = text.matches("[a-zA-z\"0-9._%+-@]*");
		else
			valid = false;
		
		return valid;
	}
	
	public static boolean isTextLengthValid(String text) {
		return text != null && !text.isEmpty() && text.length() <= 250;
	}

}
