package com.seaofnotes.util;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.Session;

import com.seaofnotes.beans.Users;

public class SessionUtil {
	
	
public static Users getCurrentUserFromDB(HttpServletRequest request) {
		
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		
		Users current = (Users) session.get(Users.class, getCurrentUser(request).getId());
		
		session.getTransaction().commit();
		session.close();
			
		return current;
	}
	
	public static Users getCurrentUser(HttpServletRequest request) {
		return (Users) request.getSession().getAttribute("cUser");
	}
	
	
	public static void setCurrentUser(Users current, HttpServletRequest request) {
		request.getSession().setAttribute("cUser", current);
	}
	
	public static void removeCurrentUser(HttpServletRequest request) {
		request.getSession().removeAttribute("cUser");
	}
	
	public static void removeSession (HttpServletRequest request) {
		request.getSession().invalidate();
	}
	
	public static boolean isCurrentUser(HttpServletRequest request) {
		return getCurrentUser(request) != null;
	}

}
