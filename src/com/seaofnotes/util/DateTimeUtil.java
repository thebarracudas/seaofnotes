package com.seaofnotes.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateTimeUtil {
	
	public static Date getDate(int year, int month, int date) {
		Calendar cal = Calendar.getInstance();
		cal.set(year, month - 1, date, 0, 0, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}
	
	public static Date getDateTime(int year, int month, int date, int hour, int minute) {
		Calendar cal = Calendar.getInstance();
		cal.set(year, month - 1, date, hour, minute, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}
	
	public static String getDateTime(Date date) {
		SimpleDateFormat d = new SimpleDateFormat("HH:mm dd.MM.yyyy");
		return d.format(date);
	}
	public static String getDate(Date date) {
		SimpleDateFormat d = new SimpleDateFormat("dd.MM.yyyy");
		return d.format(date);
	}
	public static String getTime(Date date) {
		SimpleDateFormat d = new SimpleDateFormat("HH:mm");
		return d.format(date);
	}
	
	public static Date getDateFromString(String dateString) {
		String[] validFormats = {"dd-MM-yyyy", "yyyy-MM-dd", "dd.MM.yyyy", "dd/MM/yyyy"};
		DateFormat formatter;
		Date date = null;
		
		for (String format : validFormats) {
			if(date == null){
				formatter = new SimpleDateFormat(format);
				try {
				    date = formatter.parse(dateString);
				    if(!dateString.equals(formatter.format(date))){
				    	date = null;
				    }
				} catch (ParseException e) {}
			}
			
		}
		
		return date;
	}
	
	public static Date getDateTimeFromString(String dateString) {
		DateFormat formatter = new SimpleDateFormat("dd.MM.yyyy HH:mm");
		Date date = null;
		try {
			date = formatter.parse(dateString);
		} catch (ParseException e1) {}
			
		return date;
	}

}
