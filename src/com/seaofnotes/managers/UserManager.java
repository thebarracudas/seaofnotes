package com.seaofnotes.managers;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.Session;

import com.seaofnotes.beans.Users;
import com.seaofnotes.util.GeneralUtil;
import com.seaofnotes.util.HibernateUtil;
import com.seaofnotes.util.SessionUtil;
import com.seaofnotes.util.ValidationUserUtil;

public class UserManager {

	public void saveUser(HttpServletRequest request) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();

		Users newUser = new Users();
		newUser.setEmail(request.getParameter("email"));
		newUser.setName(request.getParameter("name"));
		newUser.setSurname(request.getParameter("surname"));	    	
		newUser.setPassword(GeneralUtil.generateHash(request.getParameter("password")));
		newUser.setCreatedAt(new java.util.Date());
		newUser.setActivated(true);
		newUser.setActivatedAt(new java.util.Date());
		session.save(newUser);

		session.getTransaction().commit();

		SessionUtil.setCurrentUser(newUser, request);	
	}

	public void activateUser (Users current) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();

		current.setActivated(true);
		current.setActivatedAt(new java.util.Date());

		session.update(current);
		session.getTransaction().commit();
	}

	public void updatePassword(HttpServletRequest request) {

		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();

		Users cUser = (Users) session.merge(SessionUtil.getCurrentUser(request));

		if (ValidationUserUtil.isOldPasswordValid(request.getParameter("old_password"), cUser) &&
				ValidationUserUtil.isNewPasswordValid(request.getParameter("new_password_1"), request.getParameter("new_password_2"))) {

			cUser.setPassword(GeneralUtil.generateHash(request.getParameter("new_password_1")));

			session.update(cUser);

			session.getTransaction().commit();

			SessionUtil.setCurrentUser(cUser, request);
			request.getSession().setAttribute("successPassword", "<span class=\"glyphicon glyphicon-ok-sign\" aria-hidden=\"true\"></span> Password updated.");
		} else
			request.getSession().setAttribute("errorPassword", "<span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span> Check your data and try again! Some fields are empty or have not been accepted.");



	}

	public void updateProfile(HttpServletRequest request) {

		boolean rValue = false;

		if (ValidationUserUtil.isProfileValid(request)) {
			rValue = true;
			Session session = HibernateUtil.getSessionFactory().openSession();
			session.beginTransaction();

			Users cUser = (Users) session.merge(SessionUtil.getCurrentUser(request));

			String email = request.getParameter("email");
			if(!cUser.getEmail().equals(email) && !ValidationUserUtil.isEmailFree(email)) {
				session.getTransaction().rollback();
				request.getSession().setAttribute("errorProfile", "<span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span> Email already in use.");
				rValue = false;
			}
			if (rValue == true) {
				cUser.setEmail(request.getParameter("email"));
				cUser.setName(request.getParameter("name"));
				cUser.setSurname(request.getParameter("surname"));
				cUser.setPassword(GeneralUtil.generateHash(request.getParameter("password")));

				session.update(cUser);

				session.getTransaction().commit();

				SessionUtil.setCurrentUser(cUser, request);
				request.getSession().setAttribute("successProfile", "<span class=\"glyphicon glyphicon-ok-sign\" aria-hidden=\"true\"></span> Profile updated.");
			}
		} else
			request.getSession().setAttribute("errorProfile", "<span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span> Check your data and try again! Some fields are empty or have not been accepted.");

	}


}
