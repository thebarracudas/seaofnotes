package com.seaofnotes.managers;

import java.util.Date;
import java.util.List;

import org.hibernate.Session;

import com.seaofnotes.beans.Posts;
import com.seaofnotes.beans.Users;
import com.seaofnotes.util.HibernateUtil;

public class PostTestData {

	public static void main(String[] args) {
		
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		
		Users user1 = new Users();
		user1.setActivated(true);
		user1.setActivatedAt(new Date());
		user1.setCreatedAt(new Date());
		user1.setEmail("user1@user1.com");
		user1.setName("Name User1");
		user1.setSurname("Surname User1");
		user1.setPassword("AlfaBravo1");
		user1.setUpdatedAt(new Date());
		
		Users user2 = new Users();
		user2.setActivated(true);
		user2.setActivatedAt(new Date());
		user2.setCreatedAt(new Date());
		user2.setEmail("user2@user2.com");
		user2.setName("Name User2");
		user2.setSurname("Surname User2");
		user2.setPassword("AlfaBravo2");
		user2.setUpdatedAt(new Date());
		
		Posts post1 = new Posts();
		
		post1.setText("test1");
		post1.setPoster(user1);
		post1.setPostedAt(new Date());

		Posts post2 = new Posts();
		post2.setText("test2");
		post2.setPoster(user2);
		post2.setPostedAt(new Date());
		
		Posts post3 = new Posts();
		post3.setText("test3");
		post3.setPoster(user1);
		post3.setPostedAt(new Date());
		
		Posts post4 = new Posts();
		post4.setText("test4");
		post4.setPoster(user1);
		post4.setPostedAt(new Date());
		
		session.save(user1);
		session.save(user2);
		session.save(post1);
		session.save(post2);
		session.save(post3);
		session.save(post4);
		
		session.getTransaction().commit();
		
		session.close();

		PostManager pm = new PostManager();
		List<Posts> p = pm.getAllPosts();
		
		for(int i = 0; i < p.size()-1; i++) {
			System.out.println(p.get(i).getText());
		}

	}

}
