package com.seaofnotes.managers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;

import com.seaofnotes.beans.Posts;
import com.seaofnotes.beans.Users;
import com.seaofnotes.util.GeneralUtil;
import com.seaofnotes.util.HibernateUtil;
import com.seaofnotes.util.SessionUtil;

public class PostManager {
	
	
	public void savePost(HttpServletRequest request) {
		
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		
		Users poster = SessionUtil.getCurrentUserFromDB(request);
		
		Posts newPost = new Posts();
		
		newPost.setText(GeneralUtil.getString(request.getParameter("message")));
		newPost.setPoster(poster);
		newPost.setPostedAt(new Date());
		
		session.save(newPost);
		
		session.getTransaction().commit();
		
		request.setAttribute("having", 0);
		
	}
	
	
	@SuppressWarnings("deprecation")
	public List<Posts> getAllPosts() {
				
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		
		Criteria cr = session.createCriteria(Posts.class);
		cr.addOrder(Order.desc("postedAt"));
		
		List<?> p = cr.list();
		
		session.getTransaction().commit();
		
		List<Posts> posts = new ArrayList<Posts>();
		for(Object post : p)
			posts.add((Posts)post);
		
		return posts;

		
	}
	
	/**
	 * Get ordered Posts (only given number)
	 */
	public static List<Posts> getPosts(int having) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		
		@SuppressWarnings("deprecation")
		List <?> p = session.createCriteria(Posts.class)
				.addOrder(Order.desc("postedAt"))
				.setFirstResult(having)
		    	.setMaxResults(8)
		    	.list();

		session.getTransaction().commit();

		List<Posts> posts = new ArrayList<Posts>();
		for(Object post : p)
			posts.add((Posts)post);
		
		return posts;
	}
	


}
