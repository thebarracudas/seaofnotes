package com.seaofnotes.listeners;

import com.seaofnotes.util.HibernateUtil;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.hibernate.HibernateException;


/**
 * Application Lifecycle Listener implementation class DatabaseConnectionListener
 *
 */
@WebListener
public class DatabaseConnectionListener implements ServletContextListener {

    /**
     * Default constructor. 
     */
    public DatabaseConnectionListener() {
    }

	/**
     * @see ServletContextListener#contextDestroyed(ServletContextEvent)
     */
    public void contextDestroyed(ServletContextEvent arg0)  { 
    	HibernateUtil.getSessionFactory().close();
    }

	/**
     * @see ServletContextListener#contextInitialized(ServletContextEvent)
     */
    public void contextInitialized(ServletContextEvent arg0)  { 
    	System.out.println("Listener: establish db connection");

    	try {
			HibernateUtil.getSessionFactory().openSession();
		} catch (HibernateException e) {
			System.out.println("Listener: Session Factory error");
			e.printStackTrace();
		}    }
	
}
