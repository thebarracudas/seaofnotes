package test.java;

import static org.junit.Assert.*;

import org.junit.Test;

import com.seaofnotes.beans.Users;
import com.seaofnotes.util.LoginUtil;
import com.seaofnotes.util.ValidationUserUtil;

public class ValidationUserUtilTest {

	@Test
	public void testIsEmailValid() {
		assertTrue(ValidationUserUtil.isEmailValid("test@user.com"));
		assertFalse(ValidationUserUtil.isEmailValid(""));
		assertFalse(ValidationUserUtil.isEmailValid(null));
	}

	@Test
	public void testIsNameValid() {
		assertTrue(ValidationUserUtil.isNameValid("testABC"));
		assertTrue(ValidationUserUtil.isNameValid("test ABC"));
		assertTrue(ValidationUserUtil.isNameValid("test-ABC"));
		assertFalse(ValidationUserUtil.isNameValid("test123"));
		assertFalse(ValidationUserUtil.isNameValid("test_5"));

	}

	@Test
	public void testIsDateValid() {
		assertTrue(ValidationUserUtil.isDateValid("01.02.2016"));
		assertTrue(ValidationUserUtil.isDateValid("2013-02-01"));
		assertFalse(ValidationUserUtil.isDateValid("01. Feb 2016"));
	}

	@Test
	public void testIsPasswordValid() {
		assertFalse(ValidationUserUtil.isPasswordValid("abc"));
		assertFalse(ValidationUserUtil.isPasswordValid("12345678"));
		assertFalse(ValidationUserUtil.isPasswordValid("a12345678b"));
		assertTrue(ValidationUserUtil.isPasswordValid("Password123"));
	}

	@Test
	public void testIsNewPasswordValid() {
		assertFalse(ValidationUserUtil.isNewPasswordValid("abc", "abc"));
		assertFalse(ValidationUserUtil.isNewPasswordValid("abc123ABC", "abcdefghi"));
		assertTrue(ValidationUserUtil.isNewPasswordValid("Password123", "Password123"));
	}

}
