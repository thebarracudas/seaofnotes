package test.java;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.seaofnotes.util.ValidationPostUtil;

public class ValidationPostUtilTest {

	@Test
	public void testIsTextValid() {
		assertTrue(ValidationPostUtil.isTextValid("abc124@3465.%+-_"));
		assertFalse(ValidationPostUtil.isTextValid("(abc!?)"));
		assertFalse(ValidationPostUtil.isTextValid(""));
		assertFalse(ValidationPostUtil.isTextValid(null));
	}

	@Test
	public void testIsTextLengthValid() {
		assertTrue(ValidationPostUtil.isTextLengthValid("This is a valid lenght for a text!"));
		assertFalse(ValidationPostUtil.isTextLengthValid(null));
		assertFalse(ValidationPostUtil.isTextLengthValid(""));
		assertFalse(ValidationPostUtil.isTextLengthValid("abcdefghijklmnopqrstuvwxyz1234567890abcdefghijklmnopqrstuvwxyz1234567890abcdefghijklmnopqrstuvwxyz1234567890abcdefghijklmnopqrstuvwxyz1234567890abcdefghijklmnopqrstuvwxyz1234567890abcdefghijklmnopqrstuvwxyz1234567890abcdefghijklmnopqrstuvwxyz1234567890abcdefghijklmnopqrstuvwxyz1234567890abcdefghijklmnopqrstuvwxyz1234567890"));
	}

}
