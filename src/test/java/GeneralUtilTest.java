package test.java;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.seaofnotes.util.GeneralUtil;

public class GeneralUtilTest {

	@Test
	public void testGetLong() {

		assertEquals(new Long(12345678910L), GeneralUtil.getLong("12345678910"));
	}

	@Test
	public void testGetInteger() {
		assertEquals(new Integer(123), GeneralUtil.getInteger("123"));
		assertNull(GeneralUtil.getInteger("abc"));
	}

	@Test
	public void testGetDouble() {
		assertEquals(new Double(123.45), GeneralUtil.getDouble("123.45"));
		assertNull(GeneralUtil.getDouble("abc"));
	}

	@Test
	public void testGetString() {
		assertEquals("abc", GeneralUtil.getString("abc"));
		assertNull( GeneralUtil.getString(null));
	}

	@Test
	public void testGetBool() {
		assertTrue(GeneralUtil.getBool("abc"));
		assertFalse(GeneralUtil.getBool(null));
	}

	@Test
	public void testIsInteger() {
		assertTrue(GeneralUtil.isInteger("123"));
		assertFalse(GeneralUtil.isInteger("abc"));
	}

	@Test
	public void testIsDouble() {
		assertTrue(GeneralUtil.isDouble("123.45"));
		assertFalse(GeneralUtil.isDouble("abc"));
	}

	@Test
	public void testIsLong() {
		assertTrue(GeneralUtil.isLong("12345678910"));
		assertFalse(GeneralUtil.isLong("abc"));
	}

	@Test
	public void testRemoveComma() {
		assertEquals("abc",GeneralUtil.removeComma("abc"));
		assertEquals("abc",GeneralUtil.removeComma("a,b,c"));
	}

	@Test
	public void testGenerateUUID() {
		assertNotEquals(0, GeneralUtil.generateUUID().length());
		assertEquals(String.class, GeneralUtil.generateUUID().getClass());
	}

	@Test
	public void testGenerateHash() {
		assertNotEquals(0, GeneralUtil.generateHash("abc").length());
		assertEquals(String.class, GeneralUtil.generateHash("abc").getClass());
	}

}
