package test.java;

import static org.junit.Assert.*;

import java.sql.Time;
import java.util.Date;

import org.junit.Test;

import com.seaofnotes.util.DateTimeUtil;

public class DateTimeUtilTest {

	@SuppressWarnings("deprecation")
	@Test
	public void testGetDateIntIntInt() {
		int year = 2016;
		int month = 10;
		int date = 10;
		assertEquals(new Date(year-1900, month-1, date), DateTimeUtil.getDate(year, month, date));
	}

	@Test
	public void testGetDateTimeIntIntIntIntInt() {
		int year = 2016;
		int month = 10;
		int date = 10;
		int hour = 10;
		int minute = 10;
		assertEquals(new Date(year-1900,month-1,date, hour, minute), DateTimeUtil.getDateTime(year, month, date, hour, minute));

	}

	@Test
	public void testGetDateTimeDate() {
		int year = 2016;
		int month = 12;
		int date = 11;
		int hours = 03;
		int minutes = 14;
		Date d = new Date(year-1900, month-1, date, hours, minutes);
		assertEquals("03:14 11.12.2016", DateTimeUtil.getDateTime(d));
	}

	@Test
	public void testGetDateDate() {
		int year = 2016;
		int month = 12;
		int date = 11;
		Date d = new Date(year-1900, month-1, date);
		assertEquals("11.12.2016", DateTimeUtil.getDate(d));

	}

	@Test
	public void testGetTime() {
		int year = 2016;
		int month = 12;
		int date = 11;
		int hours = 03;
		int minutes = 14;
		Date d = new Date(year-1900, month-1, date, hours, minutes);
		assertEquals("03:14", DateTimeUtil.getTime(d));
	}

	@Test
	public void testGetDateFromString() {
		int year = 2016;
		int month = 12;
		int date = 11;
		Date d = new Date(year-1900, month-1, date);
		assertEquals(d, DateTimeUtil.getDateFromString("11.12.2016"));
		assertEquals(d, DateTimeUtil.getDateFromString("11-12-2016"));
		assertEquals(d, DateTimeUtil.getDateFromString("2016-12-11"));
		assertEquals(d, DateTimeUtil.getDateFromString("11/12/2016"));
	}

	@Test
	public void testGetDateTimeFromString() {
		int year = 2016;
		int month = 12;
		int date = 11;
		int hours = 03;
		int minutes = 14;
		Date d = new Date(year-1900, month-1, date, hours, minutes);
		assertEquals(d, DateTimeUtil.getDateTimeFromString("11.12.2016 03:14"));
	}

}
