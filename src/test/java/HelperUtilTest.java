package test.java;

import static org.junit.Assert.*;

import org.junit.Test;

import com.seaofnotes.util.HelperUtil;

public class HelperUtilTest {

	@Test
	public void testGetString() {
		assertEquals("abc", HelperUtil.getString("abc"));
		assertNull(HelperUtil.getString(null));
		assertNull(HelperUtil.getString(""));
	}

}
