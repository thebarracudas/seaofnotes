<%@include file="_header.jsp"%>

<%@include file="_navigation.jsp"%>

<c:if test="${successHome != null}">
	<div class="container">
		<div class="alert alert-success">${successHome}</div>
	</div>
</c:if>
<c:if test="${errorHome != null}">
	<div class="container">
		<div class="alert alert-danger">${errorHome}</div>
	</div>
</c:if>

<div class="container">
	<%@include file="_posts.jsp"%>
</div>

<%@include file="_footer.jsp"%>