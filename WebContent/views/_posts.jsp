<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<div class="container">
	<c:if test="${(fn:length(plist)) > 0}">
		<div id="post-list" class="row">
			<c:forEach var="i" begin="0" end="${(fn:length(plist))-1}">
				<div class="post col-md-6">
					<div class="panel panel-default">
						<div class="panel-body">
							<div class="post-text"><c:out value="${plist[i].text}"></c:out></div>
							<div class="post-info">

								<ul class="list-inline pull-right">
									<li><fmt:formatDate type="date" pattern="yyyy-MM-dd"
											value="${plist[i].postedAt}" /></li>
									<li><fmt:formatDate type="time"
											value="${plist[i].postedAt}" /></li>
								</ul>
								<span class="pull-left"><c:out
										value="${plist[i].poster.name}"></c:out></span>
							</div>
						</div>
					</div>
				</div>
			</c:forEach>
		</div>
	</c:if>

	<c:if test="${(isLI)}">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-6">
					<c:if test="${(starting >= 8)}">
						<form role="form" name="prev"
							action="${pageContext.request.contextPath}/Home" method="post">
							<button type="submit" id="less-post"
								class="more btn btn-primary btn-block pull-right">Prev</button>
							<input type="hidden" name="starting" value="${(starting-8)}" />
							<input type="hidden" name="prev" value="true" />
						</form>

					</c:if>

				</div>
				<div class="col-md-6">
					<c:if test="${((psize > 8) && ((psize-starting) > 8))}">
						<form role="form" name="next"
							action="${pageContext.request.contextPath}/Home" method="post">
							<button type="submit" id="more-post"
								class="more btn btn-primary btn-block pull-left">Next</button>
							<input type="hidden" name="starting" value="${(starting+8)}" />
							<input type="hidden" name="next" value="true" />
						</form>
					</c:if>
				</div>
			</div>
		</div>
	</c:if>
	
</div>

