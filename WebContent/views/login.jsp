<%@include file="_header.jsp"%>

<%@include file="_navigation.jsp"%>

<div class="container">
	<c:if test="${successLogin != null}">
		<div class="alert alert-success">
			${successLogin}
		</div>
	</c:if>
	<c:if test="${infoLogin != null}">
		<div class="alert alert-info">
			${infoLogin}
		</div>
	</c:if>
	<c:if test="${errorLogin != null}">
		<div class="alert alert-danger">
			${errorLogin}
		</div>
	</c:if>
</div>

<div id="login" class="container">
	<div class="center-block vertical-center">
		<div class="text-center">
			<a href="${pageContext.request.contextPath}/Home"> Sea Of Notes
			</a>
		</div>
		<form id="login-form" class="panel panel-default" action="Login" method="post" autocomplete="off" role="form">
			<c:if test="${errorLoginForm != null}">
				<div class="alert alert-danger">
					${errorLoginForm}
				</div>
			</c:if>
			<div class="form-group">
				<label for="login-email" class="sr-only">Email</label> 
				<input type="email" class="form-control" id="login-email" name="email" placeholder="email" />
			</div>
			<div class="form-group">
				<label for="login-password" class="sr-only">Password</label> 
				<input type="password" class="form-control" id="login-password" name="password" placeholder="password" />
			</div>
			<div class="form-group" id="submit">
				<input type="submit" class="btn btn-primary btn-block" value="login" />
			</div>
			<hr>
			<div id="signup" class="form-group text-center">
				or <a href="SignUp" title="sign up">Sign Up</a>
			</div>
		</form>
	</div>
</div>


<%@include file="_footer.jsp"%>