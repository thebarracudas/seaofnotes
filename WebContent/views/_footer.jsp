<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<footer id="footer-post"
	class="panel-footer navbar-fixed-bottom nav-stacked">
	<div class="container">
		<div class="row">
			<div class="col-md-7">

				<c:if test="${(isLI && (title != 'Settings'))}">
					<form id="post-form"
						action="${pageContext.request.contextPath}/Home" method="post"
						role="form">
						<div class="row">
							<div class="col-xs-10">
								<div class="form-group">
									<textarea class="form-control" id="post-box" name="message"
										maxlength="250" placeholder="write message"></textarea>
								</div>
							</div>
							<div class="col-xs-2">
								<div class="form-group" id="post-counter-submit">
									<label for="post-submit" id="post-counter"><span
										id="mcounter">0</span>/250</label> <input type="submit" name="post"
										id="post-submit"
										class="btn btn-primary btn-block center-block" value="Post" />
								</div>
							</div>
						</div>
					</form>
				</c:if>
			</div>
			<div class="col-md-5" id="align-right">
				<ul class="list-inline">
					<li class="hidden-xs">Sea Of Notes Version 2.1 &copy; 2017 by The Barracuda Team</li>
				</ul>
			</div>
		</div>
	</div>


</footer>


<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/assets/js/${script}.js"></script>


</body>
</html>