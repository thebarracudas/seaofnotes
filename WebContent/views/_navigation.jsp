
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="navbar-inverse navbar-fixed-top" role="navigation">
	<div class="container">
		<div class="navbar-header">
		
			<c:if test="${((navs != null) && (navs == true))}">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
			</c:if>
			
			<a class="navbar-brand" href="${pageContext.request.contextPath}/Home">Sea Of Notes Home</a>

		</div>
		<c:if test="${((navs != null) && (navs == true))}">
			<div class="collapse navbar-collapse">
				<ul class="nav navbar-nav navbar-right">
					<c:if test="${!isLI}">
						<li><a href="${pageContext.request.contextPath}/Login">Login</a></li>
						<li><a href="${pageContext.request.contextPath}/SignUp">Signup</a></li>
					</c:if>
					<c:if test="${isLI}">
						<li class="dropdown"><a class="dropdown-toggle"
							data-toggle="dropdown" role="button" aria-haspopup="true"
							aria-expanded="false"><c:out value="${cUser.name}"></c:out>
						</a>
							<ul class="dropdown-menu">
								<li><a href="${pageContext.request.contextPath}/Settings">Settings</a></li>
								<li role="separator" class="divider"></li>
								<li><a href="${pageContext.request.contextPath}/Logout">Log
										Out</a></li>
							</ul></li>
					</c:if>
				</ul>
			</div>
		</c:if>
	</div>
</div>