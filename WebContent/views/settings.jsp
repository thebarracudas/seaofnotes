

<%@include file="_header.jsp"%>
<%@include file="_navigation.jsp"%>


<div class="container">
	<div class="row">
		<div class="container text-center" id="title-header">
			<h1>Settings</h1>
		</div>
	</div>
</div>

<div class="container">
	<div class="form-container">
		<div class="container col-md-6">
			<div id="settings">
				<div class="center-block">
					<form id="settings-form" class="panel panel-default" action="${pageContext.request.contextPath}/Settings" method="post" role="form">
						<div class="panel-heading">
							<h1 class="panel-title">Profile</h1>
						</div>
						<div class="panel-body">
							<c:if test="${errorProfile != null}">
								<div class="alert alert-danger">${errorProfile}</div>
							</c:if>
							<c:if test="${successProfile != null}">
								<div class="alert alert-success">${successProfile}</div>
							</c:if>
							<div class="form-group has-feedback">
								<label for="settings-name">Name</label> 
								<input type="text" class="form-control" id="settings-name" name="name" value="<c:out value="${cUser.name}"></c:out>" style="text-transform: capitalize" />
								<span id="error-name" class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
								<span id="success-name" class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>
							</div>
							<div class="form-group has-feedback">
								<label for="settings-surname">Surname</label> 
								<input type="text" class="form-control" id="settings-surname" name="surname" value="<c:out value="${cUser.surname}"></c:out>" style="text-transform: capitalize" />
								<span id="error-surname" class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
								<span id="success-surname" class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>
							</div>
							<div class="form-group has-feedback">
								<label for="settings-email">Email Address</label> 
								<input type="email" class="form-control" id="settings-email" name="email" value="<c:out value="${cUser.email}"></c:out>" />
								<span id="error-email" class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
								<span id="success-email" class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>
							</div>
							<div class="form-group" id="confirm">
								<label for="settings-password_1">Confirm Password</label> 
								<input type="password" class="form-control" id="settings-password" name="password" placeholder="your password to confirm" />
							</div>
							<div class="form-group">
								<input type="submit" name="settings-submit" class="save-btn btn btn-primary btn-block center-block" value="Save" />
								<input type="hidden" name="profile-update" value="true" />
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="container col-md-6">
			<div id="password">
				<div class="center-block">
					<form id="password-form" class="panel panel-default" action="${pageContext.request.contextPath}/Settings" method="post" role="form">
						<div class="panel-heading">
							<h1 class="panel-title">Change Password</h1>
						</div>
						<div class="panel-body">
							<c:if test="${errorPassword != null}">
								<div class="alert alert-danger">${errorPassword}</div>
							</c:if>
							<c:if test="${successPassword != null}">
								<div class="alert alert-success">${successPassword}</div>
							</c:if>
							<div class="form-group">
								<label for="password-old">Old Password</label> 
								<input type="password" class="form-control" id="password-old" name="old_password" placeholder="old password" />
							</div>
							<div class="form-group has-feedback">
								<label for="password-new1">New Password</label> 
								<input type="password" class="form-control" id="password-new1" name="new_password_1" placeholder="new password" />
								<span id="help-password1" class="help-block">8 to 20 characters which contain at least one numeric digit, one uppercase and one lowercase letter.</span>
								<span id="error-password1" class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
								<span id="success-password1" class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>
							</div>
							<div class="form-group has-feedback">
								<label for="password-new2" class="sr-only">Confirm New Password</label> 
								<input type="password" class="form-control" id="password-new2" name="new_password_2" placeholder="confirm new password" />
								<span id="help-password2" class="help-block">Password doesn't match</span>
								<span id="error-password2" class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
								<span id="success-password2" class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>
							</div>
							<div class="form-group">
								<input type="submit" name="password-submit" class="save-btn btn btn-primary btn-block center-block"value="Save" />
								<input type="hidden" name="password-update" value="true" />
							</div>
						</div>
					</form>

				</div>
			</div>
		</div>
	</div>
</div>



<%@include file="_footer.jsp"%>