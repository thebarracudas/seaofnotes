<%@include file="_header.jsp"%>
<%@include file="_navigation.jsp"%>

<div id="signup" class="container">
	<div class="center-block">
		<div class="text-center">
			<a href="${pageContext.request.contextPath}/Home">
				Sea Of Notes
			</a>
		</div>
		<form id="signup-form" class="panel panel-default" action="" autocomplete="on" method="post" role="form">
			<c:if test="${errorSignup != null}">
				<div class="alert alert-danger">${errorSignup}</div>
			</c:if>
			<div class="form-group has-feedback">
				<label for="signup-name" class="sr-only">Name</label> 
				<input type="text" class="form-control" id="signup-name" name="name" placeholder="name" style="text-transform:capitalize"/>
				<span id="error-name" class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
				<span id="success-name" class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>
			</div>
			<div class="form-group has-feedback">
				<label for="signup-surname" class="sr-only">Surname</label> 
				<input type="text" class="form-control" id="signup-surname" name="surname" placeholder="surname" style="text-transform:capitalize"/>
				<span id="error-surname" class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
				<span id="success-surname" class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>
			</div>
			<div class="form-group has-feedback">
				<label for="signup-email" class="sr-only">Email address</label> 
				<input type="email" class="form-control" id="signup-email" name="email" placeholder="email" />
				<span id="error-email" class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
				<span id="success-email" class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>
			</div>
			<div class="form-group has-feedback">
				<label for="signup-password1" class="sr-only">Password</label> 
				<input type="password" class="form-control" id="signup-password1" name="password" placeholder="password"  aria-describedby="help-password"/>
				<span id="help-password1" class="help-block">8 to 20 characters which contain at least one numeric digit, one uppercase and one lowercase letter.</span>
				<span id="error-password1" class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
				<span id="success-password1" class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>
			</div>
			<div class="form-group has-feedback">
				<label for="signup-password2" class="sr-only">Password</label> 
				<input type="password" class="form-control" id="signup-password2" name="password_retype" placeholder="retype password"/>
				<span id="help-password2" class="help-block">Password doesn't match</span>
				<span id="error-password2" class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
				<span id="success-password2" class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>
			</div>
			<div class="form-group" id="submit">
				<input type="submit" name="submit" class="btn btn-primary btn-block" value="Sign up" />
			</div>
			<hr>
			<div id="login" class="form-group text-center">
				or <a href="Login" title="log in">log in</a>
			</div>
		</form>
	</div>
</div>

<%@include file="_footer.jsp"%>