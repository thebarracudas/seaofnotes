$(function() {
	$("#error-name").hide();
	$("#success-name").hide();
	$("#error-surname").hide();
	$("#success-surname").hide();
	$("#error-email").hide();
	$("#success-email").hide();
	$("#help-password1").hide();
	$("#help-password2").hide();
	$("#error-password1").hide();
	$("#success-password1").hide();
	$("#error-password2").hide();
	$("#success-password2").hide();
});

/* Name validation */
$("#settings-name").on("keyup keydown keypress", function() {
	var name = $(this).val();
	var check = /^(?=.*[a-z])(?=.*[A-Z]).{2,20}$/;  
	if(name.match(check)) {   
		$("#error-name").hide();
		$("#success-name").show();
	} else {   
		$("#error-name").show();
		$("#success-name").hide();
	}
	if(name == "") {
		$("#error-name").hide();
		$("#success-name").hide();
	}
});

/* Surname validation */
$("#settings-surname").on("keyup keydown keypress", function() {
	var surname = $(this).val();
	var check = /^(?=.*[a-z])(?=.*[A-Z]).{2,20}$/;  
	if(surname.match(check)) {   
		$("#error-surname").hide();
		$("#success-surname").show();
	} else {   
		$("#error-surname").show();
		$("#success-surname").hide();
	}
	if(surname == "") {
		$("#error-surname").hide();
		$("#success-surname").hide();
	}
});

/* Email validation */
$("#settings-email").on("keyup keydown keypress", function() {
	var email = $(this).val();
	var check = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;  
	if(email.match(check)) {   
		$("#error-email").hide();
		$("#success-email").show();
	} else {   
		$("#error-email").show();
		$("#success-email").hide();
	}
	if(email == "") {
		$("#error-email").hide();
		$("#success-email").hide();
	}
});


$("#password-new1").focusin(function() {
	$("#help-password1").show();
});

$("#password-new1").focusout(function() {
	$("#help-password1").hide();
});

/* Password validation */
$("#password-new1").on("keyup keydown keypress", function() {
	var password = $(this).val();
	var check = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,20}$/;  
	if(password.match(check)) {   
		$("#error-password1").hide();
		$("#success-password1").show();
	} else {   
		$("#error-password1").show();
		$("#success-password1").hide();
	}
	if(password == "") {
		$("#error-password1").hide();
		$("#success-password1").hide();
	}
});

/* Password-retype validation */
$("#password-new2").on("keyup keydown keypress", function() {
	var password1 = $("#password-new1").val();
	var password2 = $(this).val();
	if(password1 == password2) {
		$("#help-password2").hide();
		$("#error-password2").hide();
		$("#success-password2").show();
	} else {   
		$("#help-password2").show();
		$("#error-password2").show();
		$("#success-password2").hide();
	}
	if(password2 == "") {
		$("#help-password2").hide();
		$("#feedback-error2").hide();
		$("#success-password2").hide();
	}
});
  
