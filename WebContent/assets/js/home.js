/**
 * character counter of Post
 */

$(function() {
	if($('#post-box').length) {
		var ccounter = $('#post-box').val().length;
		if (ccounter == 0)
			$('#post-submit').prop("disabled", true);
		else
		    $('#post-submit').prop("disabled", false);
	}
});

$('#post-box').keyup(updateCounter);
$('#post-box').keydown(updateCounter);

function updateCounter() {
    var ccounter = $(this).val().length;
    $('#mcounter').text(ccounter);
    if (ccounter == 0)
    	$('#post-submit').prop("disabled", true);
    else
    	$('#post-submit').prop("disabled", false);
}

