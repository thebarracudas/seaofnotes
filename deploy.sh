#! /bin/bash
# Server prerequisites:
# sudo apt-get install git
#
# Prepare git folder in the homefolder of the connecting user:
# sudo git clone $REPO_URL
#
# Deployment:
# sudo deploy.sh
#

sudo mv ../seaofnotes.war target/seaofnotes.war
sudo docker-compose build
sudo docker-compose up -d
sudo docker-compose ps
