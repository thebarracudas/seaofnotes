#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
    CREATE USER seaofnotes WITH PASSWORD 'seame';
    CREATE DATABASE seaofnotes;
    GRANT ALL PRIVILEGES ON DATABASE seaofnotes TO seaofnotes;
EOSQL
